<?php


namespace App\Service;


use App\Entity\Order;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Message;

class MailNotifier
{
    private $mailer;
    private $smsInformer;

    public function __construct(MailerInterface $mailer, SmsInterface $sms)
    {
        $this->mailer = $mailer;
        $this->smsInformer = $sms;
    }

    public function newOrderToUser(Order $order)
    {
        $this->mailer->send(new Message('[site name] new order num: ' . $order->getId()));
        $this->smsInformer->send('new order');
    }

    public function newOrderToAdmin(Order $order)
    {
        $this->mailer->send(new Message('new order: ' . $order->getId()));
    }

    public function orderDelivered(Order $order)
    {
        $this->mailer->send(new Message('order waiting for you: ' . $order->getId()));
    }
}