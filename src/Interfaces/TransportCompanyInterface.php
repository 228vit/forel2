<?php


namespace App\Interfaces;


use App\Entity\Order;

interface TransportCompanyInterface
{
    // return TK ID
    public function createOrder(Order $order): string ;
    public function printInvoice(string $invoiceId): string ;
    public function sendOrder();
}