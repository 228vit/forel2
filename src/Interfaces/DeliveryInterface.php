<?php


namespace App\Interfaces;


use App\Entity\Order;

interface DeliveryInterface
{
    public function load(Order $order);
    public function deliver(TransportCompanyInterface $transport);
    public function unload(Order $order, Customer $customer);

}