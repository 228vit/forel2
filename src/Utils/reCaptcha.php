<?php
/**
 * Created by PhpStorm.
 * User: vit
 * Date: 05.06.18
 * Time: 14:09
 */

namespace App\Utils;


class reCaptcha
{
    /**
     * Проверяем капчу
     * @return boolean
     */
    public static function check()
    {
        $response = $_POST["g-recaptcha-response"];
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $data = array(
            'secret' => '6LekOV0UAAAAAHif3xJiDRgyFlJvDLBLhbmbuYhp',
            'response' => $response
        );
        $options = array(
            'http' => array(
                'method' => 'POST',
                'content' => http_build_query($data),
                'header' => "Content-Type: application/x-www-form-urlencoded\r\n"
            )
        );
        $context = stream_context_create($options);
        $verify = file_get_contents($url, false, $context);
        $captcha_success = json_decode($verify);

        return $captcha_success->status;
    }

}