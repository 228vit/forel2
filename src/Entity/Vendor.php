<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VendorRepository")
 */
class Vendor
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $pic;

    /**
     * @var File
     *
     * @Assert\File(mimeTypes={ "image/jpg", "image/jpeg", "image/png", "image/webp" })
     */
    private $picFile;

    /**
     * @ORM\OneToMany(targetEntity="Product", mappedBy="vendor", indexBy="id")
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity="ProductGroup", mappedBy="vendor", indexBy="id")
     */
    private $productGroups;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->productGroups = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getPic(): ?string
    {
        return null === $this->pic ? 'nopic.png' : $this->pic;
    }

    public function setPic($pic)
    {
        $this->pic = $pic;
        return $this;
    }

    public function getPicFile(): ?File
    {
        return $this->picFile;
    }

    public function setPicFile(File $picFile): self
    {
        $this->picFile = $picFile;
        return $this;
    }

    public function getProducts(): ArrayCollection
    {
        return $this->products;
    }

    public function getProductGroups(): ArrayCollection
    {
        return $this->productGroups;
    }

    public function __toString()
    {
        return $this->getName();
    }

}
