<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductSetItemRepository")
 */
class ProductSetItem
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Product one product used in many ProductSets
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="productSetItems", cascade={"persist"})
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=false, options={"default": "1"})
     */
    private $position;


    /**
     * @var ProductSet the ProductSet this row belongs (if any)
     *
     * @Assert\Type(type="App\Entity\ProductSet")
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="App\Entity\ProductSet", inversedBy="productSetItems")
     * @ORM\JoinColumn(name="product_set_id", referencedColumnName="id")
     */
    private $productSet;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProductSet(): ?ProductSet
    {
        return $this->productSet;
    }

    public function setProductSet(ProductSet $productSet): self
    {
        $this->productSet = $productSet;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): ProductSetItem
    {
        $this->position = $position;
        return $this;
    }



    public function __toString(): string
    {
        return sprintf('%s :: %s',
            $this->getProductSet()->__toString(), $this->getProduct()->__toString());
    }
}
