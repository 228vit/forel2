<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BannerRepository")
 */
class Banner
{
    const TYPE_TEXT = 'text';
    const TYPE_PIC = 'pic';
    const TYPE_COMBINED = 'combined';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", columnDefinition="ENUM('text', 'pic', 'combined')")
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="integer")
     */
    private $shows;

    /**
     * @ORM\Column(type="integer")
     */
    private $clicks;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pic;

    /**
     * @var File
     *
     * @Assert\File(mimeTypes={ "image/jpg", "image/jpeg", "image/png" })
     */
    private $picFile;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    private $content;


    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return boolval($this->isActive);
    }

    /**
     * @param bool $isActive
     * @return Banner
     */
    public function setIsActive(bool $isActive): Banner
    {
        $this->isActive = $isActive;
        return $this;
    }


    public function getPic()
    {
        return $this->pic;
    }

    public function setPic($pic): self
    {
        $this->pic = $pic;

        return $this;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getShows()
    {
        return $this->shows;
    }

    /**
     * @param mixed $shows
     * @return Banner
     */
    public function setShows($shows)
    {
        $this->shows = $shows;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClicks()
    {
        return $this->clicks;
    }

    /**
     * @param mixed $clicks
     * @return Banner
     */
    public function setClicks($clicks): self
    {
        $this->clicks = $clicks;
        return $this;
    }

    /**
     * @return File
     */
    public function getPicFile()
    {
        return $this->picFile;
    }

    /**
     * @param File $picFile
     * @return Banner
     */
    public function setPicFile(File $picFile): Banner
    {
        $this->picFile = $picFile;
        return $this;
    }

    public function __toString()
    {
        return sprintf('%s (%s)', $this->getName(), $this->getType());
    }

    public function addClick()
    {
        $this->clicks++;
    }

    public function addShows()
    {
        $this->shows++;
    }

}
