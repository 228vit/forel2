<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductSetRepository")
 */
class ProductSet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=false)
     */
    private $price;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $discount;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", options={"default": "1"})
     */
    private $isActive;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pic;

    /**
     * @var File
     *
     * @Assert\File(mimeTypes={ "image/jpg", "image/jpeg", "image/png" })
     */
    private $picFile;

    /**
     * One ProductSet has Many Products.
     * @ORM\OneToMany(targetEntity="ProductSetItem", mappedBy="productSet", cascade={"persist", "remove"})
     */
    private $productSetItems;

    public function __construct()
    {
        $this->isActive = false;
        $this->productSetItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function isActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $value): self
    {
        $this->isActive = $value;

        return $this;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPic(): ?string
    {
        return $this->pic;
    }

    public function setPic(?string $pic): self
    {
        $this->pic = $pic;

        return $this;
    }

    /**
     * @return File
     */
    public function getPicFile()
    {
        return $this->picFile;
    }

    public function setPicFile(File $picFile): ProductSet
    {
        $this->picFile = $picFile;
        return $this;
    }

    /**
     * @return int
     */
    public function getDiscount(): int
    {
        return $this->discount;
    }

    /**
     * @param int $discount
     * @return ProductSet
     */
    public function setDiscount(int $discount): ProductSet
    {
        $this->discount = $discount;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getProductSetItems()
    {
        return $this->productSetItems;
    }

    public function setProductSetItems($items)
    {
        $this->productSetItems = $items;
    }

    public function addProductSetItem(ProductSetItem $item): self
    {
        if (null === $item->getProduct()) {
            return $this;
        }

        if (!$this->productSetItems->contains($item)) {
            $item->setProductSet($this); //вот эта строка важна
            $this->productSetItems->add($item);
        }

        return $this;
    }

//    public function removeProductSetItem(ProductSetItem $item): void
//    {
//        $this->productSetItems->remove($item);
//    }

    public function __toString(): string
    {
        return $this->getName();
    }

}
