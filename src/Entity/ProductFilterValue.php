<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductFilterValueRepository")
 */
class ProductFilterValue
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="productFilterValues")
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CategoryFilterValue")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categoryFilterValue;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getCategoryFilterValue(): ?CategoryFilterValue
    {
        return $this->categoryFilterValue;
    }

    public function setCategoryFilterValue(?CategoryFilterValue $categoryFilterValue): self
    {
        $this->categoryFilterValue = $categoryFilterValue;

        return $this;
    }
}
