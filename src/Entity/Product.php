<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    CONST STATUS_STOCK = 'stock';
    CONST STATUS_TRANSIT = 'transit';
    CONST STATUS_RESERVE = 'reserve';
    CONST STATUS_ARCHIVE = 'archive';
    CONST STATUS_CALL = 'call';
    CONST STATUS_PREORDER = 'preorder';
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Category|null the group this user belongs (if any)
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="products")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @var Vendor|null the group this user belongs (if any)
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Vendor", inversedBy="products")
     * @ORM\JoinColumn(name="vendor_id", referencedColumnName="id")
     */
    private $vendor;

    /**
     * @ORM\Column(length=512)
     */
    private $name;

    /**
     * @ORM\Column(length=512)
     */
    private $fullName;

    /**
     * @ORM\Column(length=128)
     */
    private $vendorCode;

    /**
     * @ORM\Column(length=128)
     */
    private $barCode;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer")
     */
    private $price;

    /**
     * @var int
     *
     * @ORM\Column(name="price_opt", type="integer")
     */
    private $priceOpt;

    /**
     * @var int
     *
     * @ORM\Column(name="price_old", type="integer")
     */
    private $priceOld;

    /**
     * @var int
     *
     * @ORM\Column(name="price_in", type="integer", options={"default": 0})
     */
    private $priceIn;

    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true, nullable=false)
     */
    private $slug;

    /**
     * @var null|string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $warranty;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var mixed
     *
     * @ORM\Column(name="meta_description", type="text", nullable=true)
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $pic;

    /**
     * @var File
     *
     * @Assert\File(mimeTypes={ "image/jpg", "image/jpeg", "image/png", "image/webp" })
     */
    private $picFile;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_new", type="boolean", options={"default": "0"})
     */
    private $isNew;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", options={"default": "1"})
     */
    private $isActive;

    /**
     * @var bool
     *
     * @ORM\Column(name="in_action", type="boolean", options={"default": "0"})
     */
    private $inAction;

    /**
     * @var bool
     *
     * @ORM\Column(name="show_on_top", type="boolean", options={"default": "0"})
     */
    private $showOnTop;

    /**
     * @var bool
     *
     * @ORM\Column(name="show_on_market", type="boolean", options={"default": "0"})
     */
    private $showOnMarket;

    /**
     * @var string
     *
     * @ORM\Column(type="string", columnDefinition="ENUM('stock', 'call', 'transit', 'reserve', 'preorder', 'archive')", options={"default": "stock"})
     */
    private $status;

    /**
     * One Product has Many Images.
     * @ORM\OneToMany(targetEntity="ProductImage", mappedBy="product", indexBy="id", cascade={"persist"})
     */
    private $images;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductFilterValue", mappedBy="product", orphanRemoval=true)
     */
    private $productFilterValues;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductSetItem", mappedBy="product", orphanRemoval=true)
     */
    private $productSetItems;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProductGroup", inversedBy="products")
     */
    private $productGroup;

    public function __construct()
    {
        $this->isActive = true;
        $this->inAction = false;
        $this->isNew = false;
        $this->showOnTop = false;
        $this->showOnMarket = false;
        $this->status = self::STATUS_STOCK;
        $this->images = new ArrayCollection();
        $this->productFilterValues = new ArrayCollection();
        $this->productSetItems = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Product
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Category|null
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category|null $category
     * @return Product
     */
    public function setCategory(Category $category): Product
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    /**
     * @param int $price
     * @return Product
     */
    public function setPrice(int $price): Product
    {
        $this->price = $price;
        return $this;
    }

    public function getPriceIn(): ?int
    {
        return $this->priceIn;
    }

    public function setPriceIn(int $priceIn): Product
    {
        $this->priceIn = $priceIn;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): Product
    {
        $this->description = $description;
        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return empty($this->metaDescription) ? (string)$this->getName() : $this->metaDescription;
    }

    public function setMetaDescription(string $metaDescription): Product
    {
        $this->metaDescription = $metaDescription;
        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): Product
    {
        $this->slug = $slug;
        return $this;
    }

    public function getWarranty(): ?string
    {
        return $this->warranty;
    }

    public function setWarranty(string $warranty): Product
    {
        $this->warranty = $warranty;
        return $this;
    }

    public function getPic(): ?string
    {
        return null === $this->pic ? 'nopic.png' : $this->pic;
    }

    public function setPic($pic)
    {
        $this->pic = $pic;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     * @return Product
     */
    public function setIsActive(bool $isActive): Product
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return bool
     */
    public function isShowOnTop()
    {
        return $this->showOnTop;
    }

    /**
     * @param bool $showOnTop
     * @return Product
     */
    public function setShowOnTop(bool $showOnTop): Product
    {
        $this->showOnTop = $showOnTop;
        return $this;
    }

    /**
     * @return File
     */
    public function getPicFile(): ?File
    {
        return $this->picFile;
    }

    /**
     * @param File $picFile
     * @return Product
     */
    public function setPicFile(File $picFile): Product
    {
        $this->picFile = $picFile;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVendorCode(): ?string
    {
        return $this->vendorCode;
    }

    /**
     * @param mixed $vendorCode
     * @return Product
     */
    public function setVendorCode($vendorCode): ?string
    {
        $this->vendorCode = $vendorCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBarCode()
    {
        return $this->barCode;
    }

    /**
     * @param mixed $barCode
     * @return Product
     */
    public function setBarCode($barCode)
    {
        $this->barCode = $barCode;
        return $this;
    }

    /**
     * @return Vendor|null
     */
    public function getVendor(): ?Vendor
    {
        return $this->vendor;
    }

    /**
     * @param Vendor|null $vendor
     * @return Product
     */
    public function setVendor(?Vendor $vendor): Product
    {
        $this->vendor = $vendor;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status ?: Product::STATUS_STOCK;
    }

    /**
     * @param string $status
     * @return Product
     */
    public function setStatus(string $status): Product
    {
        $this->status = $status;
        return $this;
    }

    public static function getStatuses(): array
    {
        return [
            self::STATUS_STOCK => 'на складе',
            self::STATUS_CALL => 'звоните',
            self::STATUS_TRANSIT =>'в транзите',
            self::STATUS_RESERVE => 'в резерве',
            self::STATUS_PREORDER => 'под заказ',
            self::STATUS_ARCHIVE => 'снят с пр-ва',
        ];
    }

    public static function getFormStatuses(): array
    {
        return [
            'на складе' => self::STATUS_STOCK,
            'звоните' => self::STATUS_CALL,
            'в транзите' => self::STATUS_TRANSIT,
            'в резерве' => self::STATUS_RESERVE,
            'под заказ' => self::STATUS_PREORDER,
            'снят с пр-ва' => self::STATUS_ARCHIVE,
        ];
    }

    public function statusAsCssClass(): string
    {
        $css_classes = [
            self::STATUS_STOCK => 'success',
            self::STATUS_CALL => 'danger',
            self::STATUS_TRANSIT => 'primary',
            self::STATUS_PREORDER => 'danger',
            self::STATUS_RESERVE => 'danger',
            self::STATUS_ARCHIVE => 'dark',
        ];

        return $css_classes[$this->getStatus()];
    }

    public function statusRus(): string
    {
        $statuses = self::getStatuses();

        return $statuses[$this->getStatus()];
    }

    /**
     * @return mixed
     */
    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function getFullNameWithVendor(): string
    {
        return sprintf("%s - %s", $this->getVendor()->getName(), $this->fullName);
    }

    /**
     * @param mixed $fullName
     * @return Product
     */
    public function setFullName($fullName): string
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @return int
     */
    public function getPriceOpt(): ?int
    {
        return $this->priceOpt;
    }

    /**
     * @param int $priceOpt
     * @return Product
     */
    public function setPriceOpt(int $priceOpt): Product
    {
        $this->priceOpt = $priceOpt;
        return $this;
    }

    /**
     * @return bool
     */
    public function isShowOnMarket()
    {
        return (bool)$this->showOnMarket;
    }

    /**
     * @param bool $showOnMarket
     * @return Product
     */
    public function setShowOnMarket(bool $showOnMarket): Product
    {
        $this->showOnMarket = $showOnMarket;
        return $this;
    }

    public function isAvailableToOrder() : bool
    {
        $res = in_array($this->status, [
            self::STATUS_STOCK,
            self::STATUS_CALL,
            self::STATUS_PREORDER
        ]);

        return (bool) $res;
    }

    public function isInAction(): bool
    {
        return $this->inAction;
    }

    public function setInAction(bool $inAction): Product
    {
        $this->inAction = $inAction;
        return $this;
    }

    public function getPriceOld(): ?int
    {
        return $this->priceOld;
    }

    public function setPriceOld(int $priceOld): Product
    {
        $this->priceOld = $priceOld;
        return $this;
    }



    public function __toString(): string
    {
        return (string)$this->getName();
    }

    public function getImages(): Collection
    {
        return $this->images;
    }

    public function setImages(ArrayCollection $images): Product
    {
        $this->images = $images;
        return $this;
    }


    /**
     * @return Collection|ProductFilterValue[]
     */
    public function getProductFilterValues(): Collection
    {
        return $this->productFilterValues;
    }

    public function removeProductFilterValue(ProductFilterValue $productFilterValue): self
    {
        if ($this->productFilterValues->contains($productFilterValue)) {
            $this->productFilterValues->removeElement($productFilterValue);
            // set the owning side to null (unless already changed)
            if ($productFilterValue->getProduct() === $this) {
                $productFilterValue->setProduct(null);
            }
        }

        return $this;
    }

    public function getProductGroup(): ?ProductGroup
    {
        return $this->productGroup;
    }

    public function setProductGroup(?ProductGroup $productGroup): self
    {
        $this->productGroup = $productGroup;

        return $this;
    }

    /**
     * @return bool
     */
    public function isNew(): bool
    {
        return $this->isNew;
    }

    /**
     * @param bool $isNew
     * @return Product
     */
    public function setIsNew(bool $isNew): Product
    {
        $this->isNew = $isNew;
        return $this;
    }


}
