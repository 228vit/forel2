<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryFilterValueRepository")
 */
class CategoryFilterValue
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @var CategoryFilter|null the group this user belongs (if any)
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\CategoryFilter", inversedBy="filterValues")
     * @ORM\JoinColumn(name="category_filter_id", referencedColumnName="id")
     */
    private $categoryFilter;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=false, options={"default": "1"})
     */
    private $position;

    public function __construct()
    {
        $this->position = 1;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategoryFilter(): ?CategoryFilter
    {
        return $this->categoryFilter;
    }

    public function setCategoryFilter(CategoryFilter $categoryFilter): self
    {
        $this->categoryFilter = $categoryFilter;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;
        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
