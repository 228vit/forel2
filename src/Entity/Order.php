<?php

namespace App\Entity;

use App\Utils\Uuid;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Id\UuidGenerator;
/**
 * Order
 *
 * @ORM\Table(name="orders")
 * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
 */
class Order
{
    const STATUS_NEW = 'new';
    const STATUS_PREPARE = 'prepare';
    const STATUS_DELIVERY = 'delivery';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_FINISHED = 'finished';

    private $all_statuses = [];

    CONST BUYER_COMPANY = 'company';
    CONST BUYER_PERSONAL = 'personal';

    CONST DELIVERY_COURIER = 'courier';
    CONST DELIVERY_PICKUP = 'pickup';
    CONST DELIVERY_TRANSPORT = 'transport';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * One Order has Many Products as OrderItems.
     * @ORM\OneToMany(targetEntity="OrderItem", mappedBy="order")
     */
    private $items;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(type="string", columnDefinition="ENUM('company', 'personal')")
     */
    private $buyer_type;

    /**
     * @var string
     *
     * @ORM\Column(type="string", columnDefinition="ENUM('courier', 'pickup', 'transport')")
     */
    private $delivery_type;

    /**
     * @var float
     *
     * @ORM\Column(name="delivery_cost", type="float", nullable=true)
     */
    private $delivery_cost;


    /**
     * @var string
     *
     * @ORM\Column(type="string", columnDefinition="ENUM('new', 'prepare', 'delivery', 'cancelled', 'finished')")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $uuid;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="string", length=1024, nullable=true)
     */
    private $note;


    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $discountPercent;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $discountAmount;

//    /**
//     * @var \DateTime $contentChanged
//     *
//     * @ORM\Column(type="datetime", nullable=true)
//     * @Gedmo\Timestampable(on="change", field={"status", "note", "phone", "email". "address"})
//     */
//    private $contentChanged;

    public function __construct()
    {
        $this->items = new ArrayCollection();

        $this->uuid = Uuid::v4();

        // todo: похоже что это не работает ))
        $this->all_statuses = [
            self::STATUS_CANCELLED, self::STATUS_DELIVERY,
            self::STATUS_FINISHED, self::STATUS_NEW, self::STATUS_PREPARE
        ];
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }


    /**
     * Set name
     *
     * @param string $name
     *
     * @return Order
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Order
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Order
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Order
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }


    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }



    /**
     * Set status
     *
     * @param string $status
     *
     * @return Order
     */
    public function setStatus($status)
    {
        if (!in_array($status, [self::STATUS_NEW, self::STATUS_PREPARE, self::STATUS_DELIVERY, self::STATUS_FINISHED, self::STATUS_CANCELLED])) {
            throw new \InvalidArgumentException("Invalid status");
        }
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return Order
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @return mixed
     */
    public function getItems()
    {
        return $this->items;
    }


    public function addItem(OrderItem $item)
    {
        if (!$this->items->contains($item)) {
            $this->items->add($item);
        }
    }

    /**
     * @return string
     */
    public function getBuyerType(): string
    {
        return $this->buyer_type;
    }

    /**
     * @param string $buyer_type
     * @return Order
     */
    public function setBuyerType(string $buyer_type): Order
    {
        $this->buyer_type = $buyer_type;
        return $this;
    }

    public function getDeliveryType(): ?string
    {
        return $this->delivery_type;
    }

    public function getDeliveryTypeRus(): ?string
    {
        return isset($this->getDeliveryTypes()[$this->getDeliveryType()]) ?
            $this->getDeliveryTypes()[$this->getDeliveryType()] : 'unknown';
    }

    /**
     * @param string $delivery_type
     * @return Order
     */
    public function setDeliveryType(string $delivery_type): Order
    {
        $this->delivery_type = $delivery_type;
        return $this;
    }

    /**
     * @return float
     */
    public function getDeliveryCost(): float
    {
        return $this->delivery_cost;
    }

    /**
     * @param float $delivery_cost
     * @return Order
     */
    public function setDeliveryCost(float $delivery_cost): Order
    {
        $this->delivery_cost = $delivery_cost;
        return $this;
    }


    /**
     * Convert statut to Bootstrap class
     * @return string
     */
    public function getStatusCssClass()
    {
        $statuses = [
            'new' => 'primary',
            'prepare' => 'warning',
            'delivery' => 'default',
            'finished' => 'success',
            'cancelled' => 'danger',
        ];

        return isset($statuses[$this->getStatus()]) ? $statuses[$this->getStatus()] : 'default';
    }


    public static function getDeliveryTypes()
    {
        return [
            self::DELIVERY_COURIER => 'курьер',
            self::DELIVERY_PICKUP =>'самовывоз',
            self::DELIVERY_TRANSPORT => 'транспортной компанией',
        ];
    }

    public static function getFormDeliveryTypes()
    {
        return [
            'курьер' => self::DELIVERY_COURIER,
            'самовывоз' => self::DELIVERY_PICKUP,
            'транспортной компанией' => self::DELIVERY_TRANSPORT,
        ];
    }

    public function getDiscountPercent(): ?float
    {
        return $this->discountPercent;
    }

    public function setDiscountPercent(?float $discountPercent): self
    {
        $this->discountPercent = $discountPercent;

        return $this;
    }

    public function getDiscountAmount(): ?float
    {
        return $this->discountAmount;
    }

    public function setDiscountAmount(?float $discountAmount): self
    {
        $this->discountAmount = $discountAmount;

        return $this;
    }

    public function __toString()
    {
        return sprintf('Order: %s, from: %s, user: %s',
            $this->getId(), $this->getCreated()->format('Y-m-d'), $this->getName()
        );
    }

}

