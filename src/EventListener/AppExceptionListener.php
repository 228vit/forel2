<?php

namespace App\EventListener;

use App\Exception\AppExceptionInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class AppExceptionListener
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if (!$event->getException() instanceof AppExceptionInterface) {
            return;
        }

        $response = new JsonResponse($this->buildResponseData($event->getException()));
        $response->setStatusCode($event->getException()->getCode());

        $event->setResponse($response);
    }

    private function buildResponseData(AppExceptionInterface $exception)
    {
        $messages = json_decode($exception->getMessage());
        if (!is_array($messages)) {
            $messages = $exception->getMessage() ? [$exception->getMessage()] : [];
        }

        return [
            'error' => [
                'code' => $exception->getCode(),
                'messages' => $messages
            ]];
    }
}