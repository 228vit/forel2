<?php


namespace App\TransportCompanies;


use App\Entity\Order;
use App\Interfaces\TransportCompanyInterface;
use Symfony\Component\Mailer\MailerInterface;

class SDEK implements TransportCompanyInterface
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function createOrder(Order $order): string
    {
        // 1. connect API
        // 2. create ne CDEK parcel, with Order user, address, etc
        // 3. get CDEK transport Invoice
        // 4. save CDEK ID to our Order
        // 5. return CDEK Invoice ID

        return $cdekInvNumber = null;
    }

    // return file name
    public function printInvoice(string $invoiceNum): string
    {
        // 1. call API with ID and get invoice as PDF
        $pdf = null;

        return $pdf;
    }

    public function sendOrder()
    {
        // 1. stick invoice to parcel
        // 2. send parcel to CDEK office
        // 3. set Order status as sent
        // 4. send Email to Used with CDEK ID
    }
}