<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    private $cachedSlugs = [];

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findByShowOnTop()
    {
        return $this->createQueryBuilder('p')
            ->where('p.showOnTop = :show_on_top')
            ->setParameter('show_on_top', true)
            ->innerJoin('p.category', 'c')
            ->setMaxResults(6)
            ->getQuery()
            ->getResult()
        ;
    }

    public function search(string $query, $maxResults = 50)
    {
        return $this->createQueryBuilder('p')
            ->where('p.isActive = :active')
            ->setParameter('active', true)
            ->andWhere("p.name LIKE '%$query%'")
            ->innerJoin('p.category', 'c')
            ->setMaxResults($maxResults)
            ->getQuery()
            ->getResult()
        ;
    }

    public function getWithVendor()
    {
        return $this->createQueryBuilder('p')
            ->select('p, v')
            ->innerJoin('p.vendor', 'v')
            ->getQuery()
            ->getResult()
        ;
    }

    public function slugExists(string $slug): bool
    {
        if (isset($this->cachedSlugs[$slug])) {
            return $this->cachedSlugs[$slug];
        }

        $res = $this->createQueryBuilder('p')
            ->select('COUNT(p.id)')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleScalarResult();

        $res = 0 === (int)$res ? false : true;

        return $res;
    }

    public function similarProducts(Category $category, Product $product)
    {
        $nameArr = explode(' ', $product->getName());
        if (count($nameArr) > 2) {
            $similarName = $nameArr[0] . ' ' . $nameArr[1];
        } else {
            $similarName = $nameArr[0];
        }
        // todo: order by rand
        return $this->createQueryBuilder('p')
            ->where('p.category = :category_id')
            ->andWhere('p.id <> :product_id')
            ->andWhere('p.isActive = :is_active')
            ->andWhere("p.name LIKE '%$similarName%'")
            ->setParameter('category_id', $category->getId())
            ->setParameter('product_id', $product->getId())
            ->setParameter('is_active', true)
            ->innerJoin('p.category', 'c')
            ->setMaxResults(4)
            ->getQuery()
            ->getResult()
        ;
    }
}
