<?php

namespace App\Form;

use App\Entity\ProductGroup;
use Doctrine\ORM\EntityRepository;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductGroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', EntityType::class, array(
                'class' => 'App:Category',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->addOrderBy('c.root', 'ASC')
                        ->addOrderBy('c.lft', 'ASC');
                },
                'choice_label' => 'forTree',
                'expanded' => false,
            ))
            ->add('name')
            ->add('slug')
            ->add('picFile', FileType::class, array(
                'label' => 'Image file',
                'data_class' => null,
                'required' => false
            ))
            ->add('position')
            ->add('description', CKEditorType::class, [
                    'config' => array(
                        'uiColor' => '#ffffff',
                        'filebrowserBrowseRouteParameters' => array(
                            'instance' => 'default',
                            'homeFolder' => ''
                        )
                    ),
                ]
            )
//            ->add('products', EntityType::class, array(
//                'class' => 'App:Product',
//                'query_builder' => function (EntityRepository $er) {
//                    return $er->createQueryBuilder('p')
//                        ->addOrderBy('p.name', 'ASC');
//                },
//                'expanded' => true,
//                'multiple' => true,
//            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductGroup::class,
        ]);
    }
}
