<?php

namespace App\Form;

use App\Entity\Order;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('email')
            ->add('phone')
            ->add('status', ChoiceType::class, [
                'choices'  => [
                    Order::STATUS_NEW => Order::STATUS_NEW,
                    Order::STATUS_PREPARE => Order::STATUS_PREPARE,
                    Order::STATUS_DELIVERY => Order::STATUS_DELIVERY,
                    Order::STATUS_FINISHED => Order::STATUS_FINISHED,
                    Order::STATUS_CANCELLED => Order::STATUS_CANCELLED,
                ],
                'expanded' => true,
            ])
            ->add('delivery_type', ChoiceType::class, [
                'choices'  => [
                    Order::DELIVERY_PICKUP => Order::DELIVERY_PICKUP,
                    Order::DELIVERY_COURIER => Order::DELIVERY_COURIER,
                    Order::DELIVERY_TRANSPORT => Order::DELIVERY_TRANSPORT,
                ],
                'expanded' => true,
            ])
            ->add('discountPercent')
            ->add('discountAmount')
            ->add('address', TextareaType::class)
            ->add('note', TextareaType::class);

            // embedded order items
            $builder->add('items', CollectionType::class, array(
                'entry_type' => OrderItemType::class,
                'entry_options' => array('label' => false),
//                'allow_delete' => true,
//                'allow_add' => true,
//                'by_reference' => false,
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Order::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'order';
    }
}
