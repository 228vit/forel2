<?php

namespace App\Controller;

use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class YmlController extends AbstractController
{

    /**
     * @Route("/yml", name="yml")
     */
    public function index(EntityManagerInterface $em)
    {
        $catalog = $em->createQueryBuilder()
            ->select('
                c.id, c.name, 
                c.slug as categorySlug, 
                p.id as product_id, 
                p.slug as productSlug, 
                p.name as product_name, 
                p.price, p.warranty, p.pic,
                p.vendorCode, 
                v.name as vendor_name')
            ->from('App:Category', 'c')
            ->innerJoin('c.products', 'p')
            ->innerJoin('p.vendor', 'v')
            ->where('p.status = :status')->setParameter('status',Product::STATUS_STOCK)
            ->andWhere('p.isActive = :is_active')->setParameter('is_active',true)
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY)
        ;

        $categories = [];
        $products = [];

        foreach ($catalog as $item) {
            $categories[$item['id']] = $item['name'];
            $products[$item['product_id']] = [
                'name' => $item['product_name'],
                'price' => $item['price'],
                'vendor' => $item['vendor_name'],
                'vendor_code' => $item['vendorCode'],
                'category_id' => $item['id'],
                'warranty' => $item['warranty'],
                'announce' => $item['product_name'],
                'picture' => 'http://na-forel.ru/uploads/pics/'.$item['pic'],
                'url' => $this->generateUrl('product_view',[
                        'categorySlug' => $item['categorySlug'],
                        'productSlug' => $item['productSlug'],
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                )
            ];
        }

        return $this->render('yml/price-ru.html.twig', [
            'today' => date("Y-m-d H:i"),
            'categories' => $categories,
            'products' => $products,
        ]);
    }

    /**
     * @Route("/yandex/yml", name="yandex_yml")
     */
    public function yandex(EntityManagerInterface $em)
    {
        $catalog = $em->createQueryBuilder()
            ->select('c.id, c.name, c.singleName, c.pluralName,
                c.slug as categorySlug, 
                p.id as product_id, 
                p.slug as productSlug, 
                p.name as product_name, p.fullName as full_name, 
                p.description, p.price, p.warranty, p.pic,
                p.vendorCode, p.barCode, p.showOnMarket,
                v.name as vendor_name')
            ->from('App:Category', 'c')
            ->innerJoin('c.products', 'p')
            ->innerJoin('p.vendor', 'v')
            ->where('p.status = :status')->setParameter('status',Product::STATUS_STOCK)
            ->andWhere('p.isActive = :is_active')->setParameter('is_active',true)
            ->andWhere('p.showOnMarket = :show_on_market')->setParameter('show_on_market',true)
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY)
        ;

        $categories = [];
        $products = [];

        foreach ($catalog as $item) {
            $categories[$item['id']] = $item['pluralName'];
            $products[$item['product_id']] = [
                'categorySingularName' => $item['singleName'],
                'name' => $item['product_name'],
                'full_name' => $item['full_name'],
                'price' => $item['price'],
                'vendor' => $item['vendor_name'],
                'category_id' => $item['id'],
                'vendor_code' => $item['vendorCode'],
                'bar_code' => $item['barCode'],
                'warranty' => $item['warranty'],
                'description' => $item['description'],
                'picture' => !empty($item['pic']) ? 'http://na-forel.ru/uploads/pics/'.$item['pic'] : '',
                'url' => $this->generateUrl('product_view',[
                    'categorySlug' => $item['categorySlug'],
                    'productSlug' => $item['productSlug'],
                ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                )
            ];
        }

        return $this->render('yml/yandex.xml.twig', [
            'today' => date("Y-m-d H:i"),
            'categories' => $categories,
            'products' => $products,
        ], new Response('', 200, ['Content-Type' => 'application/xml']));
    }


    /**
     * @Route("/moysklad/yml", name="moysklad_yml")
     */
    public function moysklad(EntityManagerInterface $em)
    {
        $catalog = $em->createQueryBuilder()
            ->select('c.id, c.name, 
                c.slug as categorySlug, 
                p.id as product_id, 
                p.slug as productSlug, 
                p.name as product_name, p.fullName as full_name, 
                p.description, 
                p.price, 
                p.priceIn, 
                p.priceOld, 
                p.warranty, p.pic,
                p.vendorCode, p.barCode, p.showOnMarket,
                v.name as vendor_name')
            ->from('App:Category', 'c')
            ->innerJoin('c.products', 'p')
            ->innerJoin('p.vendor', 'v')
//            ->where('p.status = :status')->setParameter('status',Product::STATUS_STOCK)
            ->andWhere('p.isActive = :is_active')->setParameter('is_active',true)
            ->andWhere('p.showOnMarket = :show_on_market')->setParameter('show_on_market',true)
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY)
        ;

        $categories = [];
        $products = [];

        foreach ($catalog as $item) {
            $categories[$item['id']] = $item['name'];
            $products[$item['product_id']] = [
                'name' => $item['product_name'],
                'full_name' => $item['full_name'],
                'price' => $item['price'],
                'price_in' => $item['priceIn'],
                'price_old' => $item['priceOld'],
                'vendor' => $item['vendor_name'],
                'category_id' => $item['id'],
                'vendor_code' => $item['vendorCode'],
                'bar_code' => $item['barCode'],
                'warranty' => $item['warranty'],
                'description' => $item['description'],
                'pic' => $item['pic'],
                'picture' => 'http://na-forel.ru/uploads/pics/'.$item['pic'],
                'url' => $this->generateUrl('product_view',[
                    'categorySlug' => $item['categorySlug'],
                    'productSlug' => $item['productSlug'],
                ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                )
            ];
        }

        return $this->render('yml/moysklad.xml.twig', [
            'today' => date("Y-m-d H:i"),
            'categories' => $categories,
            'products' => $products,
        ], new Response('', 200, ['Content-Type' => 'application/xml']));
    }

    /**
     * @Route("/sitemap.xml", name="sitemap")
     */
    public function sitemap(EntityManagerInterface $em)
    {
        $catalog = $em->createQueryBuilder()
            ->select('c.id, c.name, 
                c.slug as categorySlug, 
                p.id as product_id, 
                p.slug as productSlug, 
                p.name as product_name, p.fullName as full_name, p.price, p.warranty, p.pic,
                p.vendorCode, 
                v.name as vendor_name')
            ->from('App:Category', 'c')
            ->innerJoin('c.products', 'p')
            ->innerJoin('p.vendor', 'v')
//            ->where('p.status = :status')->setParameter('status',Product::STATUS_STOCK)
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY)
        ;

        $categories = [];
        $products = [];

        foreach ($catalog as $item) {
            $categories[$item['id']] = $item['name'];
            $products[$item['product_id']] = [
                'name' => $item['product_name'],
                'full_name' => $item['full_name'],
                'url' => $this->generateUrl('product_view',[
                    'categorySlug' => $item['categorySlug'],
                    'productSlug' => $item['productSlug'],
                ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                )
            ];
        }
        $page_routes = [
            'payment' => $this->generateUrl('page_view_payment', [], UrlGeneratorInterface::ABSOLUTE_URL),
            'delivery' => $this->generateUrl('page_view_delivery', [], UrlGeneratorInterface::ABSOLUTE_URL),
            'warranty' => $this->generateUrl('page_view_warranty', [], UrlGeneratorInterface::ABSOLUTE_URL),
            'contacts' => $this->generateUrl('page_view_contacts', [], UrlGeneratorInterface::ABSOLUTE_URL),
//            'about' => $this->generateUrl('page_view_about', UrlGeneratorInterface::ABSOLUTE_URL),
        ];


        return $this->render('yml/sitemap.html.twig', [
            'today' => date("Y-m-d"),
            'products' => $products,
            'pages' => $page_routes,
            'server_name' => sprintf('%s://%s', $_SERVER['REQUEST_SCHEME'], $_SERVER['SERVER_NAME']),
        ]);
    }
}
