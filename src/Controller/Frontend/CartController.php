<?php

namespace App\Controller\Frontend;

use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Product;
use App\Repository\OrderRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CartController extends AbstractController
{
    /**
     * @Route("/cart/add/{id}", name="add_to_cart")
     */
    public function addToCart(Request $request, Product $product)
    {
        if (!$product) {
            throw new NotFoundHttpException();
        }

        $session = $this->get('session');
        $cart = $session->get('cart');
        if (isset($cart[$product->getId()])) {
            $cart[$product->getId()]++;
        } else {
            $cart[$product->getId()] = 1;
        }

        $session->set('cart', $cart);
        $session->getFlashBag()->add('success', $product->getName() . ' добавлен в Вашу корзину.');

        $referer = $request->headers->get('referer');

        // todo: handle AJAX request and return JSON response
        return new RedirectResponse($referer);
    }

    /**
     * @Route("/cart/update/{mode}/{product_id}", name="cart_item_update")
     */
    public function updateCartItem(Request $request, EntityManagerInterface $em, $mode, $product_id)
    {
        /** @var Product $product */
        $product = $em->getRepository('App:Product')->findOneBy(['id' => $product_id]);

        if (!$product) {
            throw new NotFoundHttpException();
        }

        $session = $this->get('session');
        $cart = $session->get('cart');
        if (isset($cart[$product_id])) {
            if ($mode == 'increase') {
                $cart[$product_id]++;
            } elseif ($mode == 'decrease') {
                $cart[$product_id]--;
                if ($cart[$product_id] <= 0) {
                    unset($cart[$product_id]);
                }
            }
        }

        $session->set('cart', $cart);
        $session->getFlashBag()->add('success', 'Ваша корзина обновлена.');

        $referer = $request->headers->get('referer');

        // todo: handle AJAX request and return JSON response
        return new RedirectResponse($referer);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @Route("/cart/remove/{id}", name="cart_remove_item")
     */

    public function removeFromCartAction(Request $request, $id)
    {
        $session = $this->get('session');
        $cart = $session->get('cart', false);

        if (is_array($cart) && ($cart[$id])) {
            unset($cart[$id]);
            $session->set('cart', $cart);
            $this->addFlash('success', 'Товар удалён из корзины');
        } else {
            $this->addFlash('error', 'Неверный параметр');
        }

        $referer = $request->headers->get('referer');
        // todo: AJAX request and JSON response
        return new RedirectResponse($referer);
    }

    /**
     * @Route("/cart/show", name="cart_show")
     */
//    public function showCart(Request $request)
//    {
//        $session = $this->get('session');
//        $cart = $session->get('cart');
//
//        return $this->render('cart/checkout.html.twig', [
//            'cart' => $cart_arr,
//            'total' => $total,
//            'server_name' => sprintf('%s://%s', $_SERVER['REQUEST_SCHEME'], $_SERVER['SERVER_NAME']),
//        ]);
//
//    }

    public function showCartDropDown()
    {
        $em = $this->getDoctrine()->getManager();
        $session = $this->get('session');
        $cart = $session->get('cart');
        $product_ids = is_array($cart) ? array_keys($cart) : array();
        $cart_total_qty = 0;
        $cart_total_sum = 0;

        $query = $em->createQuery('SELECT p, c FROM App:Product p INNER JOIN p.category c WHERE p.id IN (:ids)');
        $query->setParameter('ids', $product_ids);
        $products = $query->getResult();

        $cart_arr = [];

        /** @var Product $product */
        foreach ($products as $product) {
            $cart_item_qty = $cart[$product->getId()];
            $cart_total_qty += $cart_item_qty;
            $cart_total_sum += $product->getPrice() * $cart_item_qty;

            // todo: make a class Cart, not an array
            $cart_arr[$product->getId()]['product'] = array(
                'id' => $product->getId(),
                'name' => $product->getName(),
                'price' => $product->getPrice(),
                'pic' => $product->getPic(),
            );

            $cart_arr[$product->getId()]['qty'] = $cart[$product->getId()];
        }

        return $this->render('cart/cart_as_dropdown.html.twig', [
            'cart' => $cart_arr,
            'cart_total_qty' => $cart_total_qty,
            'cart_total_sum' => $cart_total_sum,
        ]);
    }


    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     * @Route("/buy/{id}", name="buy_in_one_click")
     */
    public function buyOneClickAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var Product $product */
        $product = $em->getRepository(Product::class)->find($id);

        if (!$product) {
            throw new NotFoundHttpException();
        }

        // todo: может без корзины пересылать на форму оформления?
        $session = $this->get('session');
        $cart = $session->get('cart');
        $cart[$id] = 1;
//        if (isset($cart[$id])) {
//            $cart[$id] = 1;
//        } else {
//            throw new NotFoundHttpException();
//        }
        $session->set('cart', $cart);

        return $this->redirectToRoute('checkout');
    }

    /**
     * Отправить корзину по почте
     *
     * @param Request $request
     * @Route("/checkout/mail", name="checkout_mail")
     */
    public function checkoutMailAction(Request $request, \Swift_Mailer $mailer)
    {
        $session = $this->get('session');
        $cart = $session->get('cart', false);
        if (!$cart) {
            return new JsonResponse([], 400);
        }
        $product_ids = array_keys($cart);

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery('SELECT p, c FROM App:Product p INNER JOIN p.category c WHERE p.id IN (:ids)');
        $query->setParameter('ids', $product_ids);
        $products = $query->getResult();

        $cart_arr = [];
        $total = 0;
        /** @var Product $product */
        foreach ($products as $product) {
            // todo: make a class Cart not an array
            $cart_arr[$product->getId()]['product'] = $product;
            $cart_arr[$product->getId()]['qty'] = $cart[$product->getId()];
            $cart_arr[$product->getId()]['amount'] = $amount = round($cart[$product->getId()] * $product->getPrice(), 2);
            $total += $amount;
        }

        if ($total > 1000 AND $total < 5000) {
            $discountPercent = 3;
        } else if ($total >= 5000 AND $total < 10000) {
            $discountPercent = 5;
        } else if ($total >= 10000) {
            $discountPercent = 10;
        } else {
            $discountPercent = 0;
        }
        $discountAmount = ($discountPercent > 0) ? round(($total/100) * $discountPercent, 2) : 0;

        $message = (new \Swift_Message('checkout'))
            ->setFrom('noreply@na-forel.ru')
            ->setTo('228vit@gmail.com')
            ->setBody(
                $this->renderView('cart/mail_new_checkout.html.twig',
                    array(
                        'ip' => $request->getClientIp(),
                        'browser' => $_SERVER['HTTP_USER_AGENT'],
                        'cart' => $cart_arr,
                        'total' => $total,
                        'discountPercent' => $discountPercent,
                        'discountAmount' => $discountAmount,
                    )
                ),
                'text/html'
            )
        ;
        $success_cnt = $mailer->send($message);

        return new JsonResponse([]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse | Response
     * @Route("/checkout", name="checkout")
     */
    public function checkoutAction(Request $request,
                                   SessionInterface $session,
                                   MailerInterface $mailer,
                                   EntityManagerInterface $em)
    {
        $cart = $session->get('cart', false);

        if (!$cart) {
            $session->getFlashBag()->add('success', 'Ваша корзина пуста');

            // todo: AJAX request and JSON response

            $referer = $request->headers->get('referer');
            if (strpos('/cart/show', $referer)) {
                $session->getFlashBag()->add('success', 'Ваша корзина пуста');
                return new RedirectResponse($referer);
            } else {
                return $this->redirectToRoute('homepage');
            }
        }

        $product_ids = array_keys($cart);

        $query = $em->createQuery('SELECT p, c FROM App:Product p INNER JOIN p.category c WHERE p.id IN (:ids)');
        $query->setParameter('ids', $product_ids);
        $products = $query->getResult();

        $cart_arr = [];
        $total = 0;
        /** @var Product $product */
        foreach ($products as $product) {
            // todo: make a class Cart not an array
            $cart_arr[$product->getId()]['product'] = $product;
            $cart_arr[$product->getId()]['qty'] = $cart[$product->getId()];
            $cart_arr[$product->getId()]['amount'] = $amount = round($cart[$product->getId()] * $product->getPrice(), 2);
            $total += $amount;
        }

        if ($total > 1000 AND $total < 5000) {
            $discountPercent = 3;
        } else if ($total >= 5000 AND $total < 10000) {
            $discountPercent = 5;
        } else if ($total >= 10000) {
            $discountPercent = 10;
        } else {
            $discountPercent = 0;
        }
        $discountAmount = ($discountPercent > 0) ? round(($total/100) * $discountPercent, 2) : 0;

        $message = (new Email())
            ->subject('[na-forel] checkout')
            ->from('noreply@na-forel.ru')
            ->to('228vit@gmail.com')
            ->cc('info@na-forel.ru')
            ->html($this->renderView('cart/mail_new_checkout.html.twig', [
                'ip' => $request->getClientIp(),
                'browser' => $_SERVER['HTTP_USER_AGENT'],
                'cart' => $cart_arr,
                'total' => $total,
                'discountPercent' => $discountPercent,
                'discountAmount' => $discountAmount,
            ]));

        try {
            $mailer->send($message);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], 500);
        }

        return $this->render('cart/checkout.html.twig', [
            'cart' => $cart_arr,
            'discountPercent' => $discountPercent,
            'discountAmount' => $discountAmount,
            'total' => $total,
            'server_name' => sprintf('%s://%s', $_SERVER['REQUEST_SCHEME'], $_SERVER['SERVER_NAME']),
        ]);
    }


    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     * @Route("/order", name="place_order")
     */
    public function orderAction(Request $request,
                                SessionInterface $session,
                                MailerInterface $mailer,
                                EntityManagerInterface $em)
    {
        $cart = $this->checkoNotEmptyShoppingCart($request, $session);

        $product_ids = array_keys($cart);

        // create order
        $order = new Order();
        // todo: use form for validation
        $order->setName($request->request->get('name'));
        $order->setNote($request->request->get('note'));
        $order->setEmail($request->request->get('email'));
        $order->setPhone($request->request->get('phone'));
        $order->setAddress($request->request->get('address'));
        $order->setNote($request->request->get('note'));
        $order->setDeliveryType($request->request->get('delivery_type'));
        $order->setAddress($request->request->get('address'));
        $order->setStatus(Order::STATUS_NEW);
        $em->persist($order);
        $em->flush();
        // create order details

        $cart_arr = [];
        $total = 0;
        $totalQty = 0;

        $query = $em->createQuery('SELECT p, c FROM App:Product p INNER JOIN p.category c WHERE p.id IN (:ids)');
        $query->setParameter('ids', $product_ids);
        $products = $query->getResult();

        /** @var Product $product */
        foreach ($products as $product) {
            $order_item = new OrderItem();
            // todo: make setOrder
            $order_item->setOrder($order);
            $order_item->setProduct($product);
            $order_item->setPrice($product->getPrice());
            $order_item->setQty($cart[$product->getId()]);
            $em->persist($order_item);
            $em->flush();

            // todo: make a class Cart not an array
            $cart_arr[$product->getId()]['product'] = $product;
            $cart_arr[$product->getId()]['qty'] = $cart[$product->getId()];
            $cart_arr[$product->getId()]['amount'] = $amount = round($cart[$product->getId()] * $product->getPrice(), 2);
            $total += $amount;
            $totalQty += $cart[$product->getId()];
        }
        if ($total > 1000 AND $total < 5000) {
            $discountPercent = 3;
        } else if ($total >= 5000 AND $total < 10000) {
            $discountPercent = 5;
        } else if ($total >= 10000) {
            $discountPercent = 10;
        } else {
            $discountPercent = 0;
        }
        $discountAmount = ($discountPercent > 0) ? round(($total/100) * $discountPercent, 2) : 0;

        $order->setDiscountPercent($discountPercent)
            ->setDiscountAmount($discountAmount);
        $em->persist($order);
        $em->flush();

        $message = (new Email())
            ->subject('[na-forel] new order '.$order->getId())
            ->from('noreply@na-forel.ru')
            ->to('228vit@gmail.com')
            ->cc('info@na-forel.ru')
            ->html($this->renderView('cart/mail_new_order.html.twig', [
                'order' => $order,
                'cart' => $cart_arr,
                'total' => $total,
                'totalQty' => $totalQty,
                'discountPercent' => $discountPercent,
                'discountAmount' => $discountAmount,
            ]));

        $userMessage = $message;
        $userMessage->to($order->getEmail())
            ->cc(null);

        try {
            $mailer->send($message);
            $mailer->send($userMessage);
        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], 500);
        }

        // clear cart!!!
        $session->set('cart', false);

        $url = $this->generateUrl('order_success', ['uuid' => $order->getUuid()]);
        return new RedirectResponse($url);
    }

    /**
     * @Route("/order/success/{uuid}", name="order_success")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function orderSuccess(OrderRepository $repository, EntityManagerInterface $em, $uuid)
    {
        $order = $repository->findOneBy(['uuid' => $uuid]);

        if (!$order) {
            $this->addFlash('error','Неверный параметр');

            return $this->redirectToRoute('homepage');
        }

        return $this->render('cart/order_success.html.twig', [
            'order' => $order,
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/cart/delete/item", name="cart_delete_item")
     */
    public function cartDeleteItemAction(Request $request, SessionInterface $session)
    {
        $id = $request->query->get('id');
        $cart = $session->get('cart');

        unset($cart[$id]);

        $session->set('cart', $cart);
        $session->getFlashBag()->add('success', 'Ваша корзина обновлена');

        return new JsonResponse(['status' => 'ok', 'cart_items_cnt' => count($cart)]);
    }

    public function renderCart(SessionInterface $session)
    {
        $cart = $session->get('cart');

        return $this->render('cart/cart_in_header.html.twig', [
            'rows' => $cart
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    private function checkoNotEmptyShoppingCart(Request $request, SessionInterface $session)
    {
        $cart = $session->get('cart', false);

        if (!$cart) {
            throw new \Exception('Your shopping cart is empty.');

//            $session->getFlashBag()->add('error', 'Your shopping cart is empty.');
//
//            // todo: AJAX request and JSON response
//
//            $referer = $request->headers->get('referer');
//            if (strpos('/cart/show', $referer)) {
//                $session->getFlashBag()->add('error', 'Your shopping cart is empty.');
//                return new RedirectResponse($referer);
//            } else {
//                return $this->redirectToRoute('homepage');
//            }
        }

        return $cart;
    }
}
