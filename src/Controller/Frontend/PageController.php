<?php

namespace App\Controller\Frontend;

use App\Entity\Page;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PageController extends AbstractController
{

    /**
     * @Route("/contacts", name="page_view_contacts")
     */
    public function contactsAction()
    {
        return $this->viewAction('contacts');

        $em = $this->getDoctrine()->getManager();

        $page = $em->getRepository(Page::class)->findOneBy(['slug' => 'contacts']);

        if (!$page) {
            $page = new Page();
        }

        return $this->render('new/page/contacts.html.twig', array(
            'page' => $page,
        ));
    }

    /**
     * @Route("/delivery", name="page_view_delivery")
     */
    public function deliveryAction(Request $request)
    {
        return $this->render('page/view.html.twig', array(
            'page' => $this->getPage('delivery'),
        ));
//        return $this->render('page/delivery.html.twig', array(
//            'server_name' => sprintf('%s://%s', $_SERVER['REQUEST_SCHEME'], $_SERVER['SERVER_NAME']),
//            'page' => $this->getPage('delivery'),
//        ));
    }

    /**
     * @Route("/warranty", name="page_view_warranty")
     */
    public function warranty(Request $request)
    {
        return $this->viewAction('warranty');
    }

    /**
     * @Route("/services", name="page_view_services")
     */
    public function servicesAction(Request $request)
    {
        return $this->viewAction('services');
    }

    /**
     * @Route("/payment", name="page_view_payment")
     */
    public function paymentAction(Request $request)
    {
        return $this->viewAction('payment');
    }

    /**
     * @Route("/dealer", name="page_view_dealers")
     */
    public function dealer(Request $request)
    {
        return $this->viewAction('dealers');
    }

    /**
     * @Route("/page/{slug}", name="page_view")
     */
    public function pageViewAction($slug, Request $request)
    {
        return $this->viewAction($slug);
    }

    private function viewAction($slug)
    {
        return $this->render('page/view.html.twig', array(
            'page' => $this->getPage($slug),
        ));
    }

    private function getPage($slug)
    {
        $em = $this->getDoctrine()->getManager();

        $page = $em->getRepository(Page::class)->findOneBy(['slug' => $slug]);

        if (!$page) {
            throw $this->createNotFoundException('The page not exist');
        }

        return $page;
    }
}