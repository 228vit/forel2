<?php

namespace App\Controller\Frontend;

use App\Entity\Feedback;
use App\Form\FeedbackType;
use App\Form\SubscriptionType;
use App\Utils\reCaptcha;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SubscriptionController extends AbstractController
{
    /**
     * @Route("/subscription/success", name="subscription_success")
     * @Method("GET")
     */
    public function success(Request $request)
    {
        return $this->render('subscription/success.html.twig', [
        ]);
    }

    /**
     * @Route("/subscription", name="subscription_new")
     * @Method("GET")
     */
    public function new(Request $request)
    {
        $form = $this->createForm(SubscriptionType::class);

        return $this->render('subscription/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/subscription", name="subscription_create")
     * @Method("POST")
     */
    public function create(Request $request, \Swift_Mailer $mailer)
    {
        $form = $this->createForm(SubscriptionType::class);

        $form->handleRequest($request);

        $recaptcha = $request->request->get('g-recaptcha-response', 'not set');
//        die('recaptcha:'.$recaptcha);
        if ($form->isSubmitted() && $form->isValid()) {

            // get email from form
            $email = $form->getData()['email'];

            $message = (new \Swift_Message('new subscription ' . $email))
                ->setFrom('noreply@na-forel.ru')
                ->setTo('info@na-forel.ru')
                ->setBody($email,'text/plain')
            ;
            $success_cnt = $mailer->send($message);

            $this->addFlash('success','Subscription success!');

            return new RedirectResponse($this->generateUrl('subscription_success'));
        }

        $this->addFlash('error','Error due creating subscription!');

        return $this->render('subscription/new.html.twig', [
                'form' => $form->createView(),
        ]);
    }

}
