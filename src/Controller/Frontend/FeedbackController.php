<?php

namespace App\Controller\Frontend;

use App\Entity\Feedback;
use App\Form\FeedbackType;
use App\Utils\reCaptcha;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FeedbackController extends AbstractController
{
    /**
     * @Route("/feedback/success", name="feedback_success")
     */
    public function success(Request $request)
    {
        return $this->render('feedback/success.html.twig', [
        ]);
    }

    public function shortFeedbackForm()
    {
        $feedback = new Feedback();
        $form = $this->createForm(FeedbackType::class, $feedback);

        return $this->render('feedback/footer_form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/feedback/create", name="feedback_create", methods={"POST"})
     */
    public function create(Request $request, MailerInterface $mailer, EntityManagerInterface $em)
    {
        $isAjax = $request->isXmlHttpRequest();

        if (!$isAjax) {}

        $feedback = new Feedback();
        $form = $this->createForm(FeedbackType::class, $feedback);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) { //  && reCaptcha::check()

            $em->persist($feedback);
            $em->flush();

            $email = (new Email())
                ->subject('[na-forel] new feedback')
                ->from('noreply@na-forel.ru')
                ->to(new Address('info@na-forel.ru'))
                ->replyTo(new Address($feedback->getEmail()))
                ->text($feedback->getMessage())
            ;

            try {
                $mailer->send($email);
                $this->addFlash('success','Feedback success!');
            } catch (\Exception $e) {
                return new JsonResponse(['error' => $e->getMessage()], 500);
            }


            return $isAjax ?
                new JsonResponse(['status' => 'success']) :
                new RedirectResponse($this->generateUrl('feedback_success'));
        }

        $this->addFlash('danger','Ошибка при отправке формы.');

        return $isAjax ?
            // todo: extract form errors
            new JsonResponse(['status' => 'fail'], 400) :
            $this->render('feedback/new.html.twig', [
                'form' => $form->createView(),
            ])
        ;
    }

}
