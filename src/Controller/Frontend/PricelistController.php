<?php


namespace App\Controller\Frontend;


use App\Entity\Category;
use App\Entity\Product;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class PricelistController extends AbstractController
{

    /**
     * Displays a form to edit an existing product entity.
     *
     * @Route("/pricelist", name="pricelist")
     * @Method({"GET"})
     * @param Request $request
     * @return Response
     */
    public function pricelist(Request $request)
    {
        $styleBold = [
            'font' => [
                'bold' => true,
            ],
        ];
        $styleCentered = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ],
        ];
        $styleBoldBordered = [
            'font' => [
                'bold' => true,
            ],
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
        ];

        $pricelistFileName = __DIR__.'../../public/uploads/pricelist.xlsx';
        try {
            $rawBody = file_get_contents($pricelistFileName);
        } catch (\Exception $e) {
        }

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var CategoryRepository $repo */
        $repo = $em->getRepository(Category::class);

        /** @var ProductRepository $repo */
        $prodRepo = $em->getRepository(Category::class);

//        $rows = $repo->findAllJoinProducts();

        $query = $em->createQuery(
            'SELECT c, p, v
                FROM App\Entity\Category c
                INNER JOIN c.products p
                INNER JOIN p.vendor v
                WHERE p.isActive = :isActive
                ORDER BY c.id ASC, v.name ASC, p.name ASC '
        )
            ->setParameter('isActive', true)
        ;

        // returns an array of Product objects
        $categories = $query->execute();


        $spreadsheet = new Spreadsheet();
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(50);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(14);

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Na-Forel.RU');
        $sheet->setCellValue('B1', '+7 916 572 7715');
        $spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($styleBold);

        $i = 2;
        $sheet->setCellValue('A'.++$i, "Название");
        $sheet->setCellValue('B'.$i, "Цена");
        $sheet->setCellValue('C'.$i, "Опт.");
        $sheet->setCellValue('D'.$i, "Гарантия");
        $spreadsheet->getActiveSheet()->getStyle("B{$i}:D{$i}")->applyFromArray($styleCentered);
        $spreadsheet->getActiveSheet()->getStyle("A{$i}:D{$i}")->applyFromArray($styleBoldBordered);

        /** @var Category $category */
        foreach ($categories as $category) {
            $sheet->setCellValue('A'.++$i, $category->getName());
            $spreadsheet->getActiveSheet()->getStyle("A{$i}:D{$i}")->applyFromArray($styleBoldBordered);

            /** @var Product $product */
            foreach ($category->getProducts() as $product) {
                $sheet->setCellValue('A'.++$i, $product->getVendor()->getName() . ' - ' . $product->getName());
                $sheet->setCellValue('B'.$i, $product->getPrice());
                $sheet->setCellValue('C'.$i, $product->getPriceOpt());
                $sheet->setCellValue('D'.$i, $product->getWarranty());
                $spreadsheet->getActiveSheet()->getStyle("D{$i}")->applyFromArray($styleCentered);
            }
        }

        $writer = new Xlsx($spreadsheet);
        try {
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header("Content-Disposition: attachment;filename=naforel-pricelist.xlsx");
            header('Cache-Control: max-age=0');
            $writer->save('php://output');
            $writer->save($pricelistFileName);
        } catch (\Exception $e) {
            die($e->getMessage());
        }

    }

}