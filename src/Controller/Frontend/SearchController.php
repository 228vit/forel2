<?php


namespace App\Controller\Frontend;

use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SearchController extends AbstractController
{
    /**
     * @Route("/search", name="front_search")
     * @Method({"GET", "POST"})
     */
    public function searchAction(ProductRepository $productRepository, Request $request)
    {
        if (null === $query = $request->query->get('q', null)) {
            $this->addFlash('alert', 'Пустой поиск.');
            return $this->redirectToRoute('homepage');
        }

        $products = $productRepository->search($query);

        return $this->render('common/search.html.twig', [
            'products' => $products,
        ]);
    }
}