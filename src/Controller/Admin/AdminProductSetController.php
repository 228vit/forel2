<?php


namespace App\Controller\Admin;

use App\Entity\ProductSet;
use App\Entity\ProductSetItem;
use App\Filter\ProductSetFilter;
use App\Form\ProductSetType;
use App\Repository\ProductRepository;
use App\Repository\ProductSetRepository;
use App\Service\FileUploader;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AdminProductSetController extends AbstractController
{
    use AdminTraitController;
    private CONST ROWS_PER_PAGE = 10;
    private CONST MODEL = 'productset';
    private CONST ENTITY_NAME = 'ProductSet';
    private CONST NS_ENTITY_NAME = 'App:ProductSet';

    /**
     * Lists all product entities.
     *
     * @Route("backend/productset/index", name="backend_productset_index", methods={"GET"})
     */
    public function indexAction(Request $request, SessionInterface $session)
    {
        $pagination = $this->getPagination($request, $session, ProductSetFilter::class);

        return $this->render('admin/productset/index.html.twig', array(
            'pagination' => $pagination,
            'current_filters' => $this->current_filters,
            'filter_form' => $this->filter_form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
            'list_fields' => [
                'a.id' => [
                    'title' => 'ID',
                    'row_field' => 'id',
                    'sorting_field' => 'product.id',
                    'sortable' => true,
                ],
                'a.name' => [
                    'title' => 'Name',
                    'row_field' => 'name',
                    'sorting_field' => 'product.name',
                    'sortable' => true,
                ],
                'a.price' => [
                    'title' => 'Price',
                    'row_field' => 'price',
                    'sorting_field' => 'product.price',
                    'sortable' => true,
                ],
                'a.pic' => [
                    'title' => 'Pic',
                    'row_field' => 'pic',
                    'sorting_field' => 'product.pic',
                    'sortable' => false,
                ],
                'a.isActive' => [
                    'title' => 'Is active?',
                    'row_field' => 'isActive',
                    'sorting_field' => 'product.isActive',
                    'sortable' => false,
                ],
            ]
        ));
    }


    /**
     * Creates a new product entity.
     *
     * @Route("backend/productset/new", name="backend_productset_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request, ProductRepository $productRepository,
                              ValidatorInterface $validator, FileUploader $fileUploader)
    {
        $product = new ProductSet();
        $form = $this->createForm(ProductSetType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $product->getPicFile();

            if (null !== $file) {
                $fileName = $fileUploader->upload($file);
                $product->setPic($fileName);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush($product);

            $this->addFlash('success', 'New record was created!');

            return $this->redirectToRoute('backend_productset_edit', array('id' => $product->getId()));
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'Errors due creating object!');
        }

        return $this->render('admin/common/new.html.twig', array(
            'product' => $product,
            'form' => $form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,

        ));
    }

    /**
     * Displays a form to edit an existing product entity.
     *
     * @Route("backend/productset/{id}/edit", name="backend_productset_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, int $id,
                               ProductSetRepository $productSetRepository,
                               FileUploader $fileUploader
    )
    {
        $productSet = $productSetRepository->getWithSetItemsOrdered($id);

        $originalItems = new ArrayCollection();

        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($productSet->getProductSetItems() as $item) {
            $originalItems->add($item);
        }

        $form = $this->createForm(ProductSetType::class, $productSet);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $productSet->getPicFile();

            if (null !== $file) {
                $fileName = $fileUploader->upload($file);
                $productSet->setPic($fileName);
            }

            // remove deleted items
            foreach ($originalItems as $item) {
                if (false === $productSet->getProductSetItems()->contains($item)) {
                    $em->remove($item);
                }
            }

            // todo: это должно быть в модели
            foreach ($productSet->getProductSetItems() as $productSetItem) {
                if (null === $productSetItem->getProductSet()) {
                    $productSetItem->setProductSet($productSet);
                }
            }

            $em->persist($productSet);
            $em->flush();

            $this->addFlash('success', 'Your changes were saved!');

            return $this->redirectToRoute('backend_productset_edit', array('id' => $productSet->getId()));
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'Errors due saving object!');
        }

        $deleteForm = $this->createDeleteForm($productSet);

        return $this->render('admin/productset/edit.html.twig', array(
            'row' => $productSet,
            'productSet' => $productSet,
//            'productFilters' => $productSetFilters,
//            'categoryFilters' => $categoryFilters,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
        ));
    }

    /**
     * Deletes a product entity.
     *
     * @Route("backend/productset/{id}", name="backend_productset_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, ProductSet $product)
    {
    }


    /**
     * Creates a form to delete a product entity.
     *
     * @param ProductSet $product The product entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ProductSet $product)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_productset_delete', array('id' => $product->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }

}