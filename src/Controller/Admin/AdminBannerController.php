<?php

namespace App\Controller\Admin;

use App\Entity\Banner;
use App\Filter\BannerFilter;
use App\Repository\BannerRepository;
use App\Service\FileUploader;
use Doctrine\ORM\Query;
use Knp\Component\Pager\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class AdminBannerController extends AbstractController
{
    use AdminTraitController;

    CONST ROWS_PER_PAGE = 10;
    CONST MODEL = 'banner';
    CONST ENTITY_NAME = 'Banner';
    CONST NS_ENTITY_NAME = 'App:Banner';

    /**
     * Lists all banner entities.
     *
     * @Route("backend/banner/index", name="backend_banner_index")
     * @Method("GET")
     */
    public function indexAction(Request $request, SessionInterface $session)
    {
        $pagination = $this->getPagination($request, $session, BannerFilter::class);

        return $this->render('admin/banner/index.html.twig', array(
            'pagination' => $pagination,
            'current_filters' => $this->current_filters,
            'filter_form' => $this->filter_form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
            'list_fields' => [
                'a.id' => [
                    'title' => 'ID',
                    'row_field' => 'id',
                    'sorting_field' => 'banner.id',
                    'sortable' => true,
                ],
                'a.name' => [
                    'title' => 'Name',
                    'row_field' => 'name',
                    'sorting_field' => 'banner.name',
                    'sortable' => true,
                ],
                'a.type' => [
                    'title' => 'Type',
                    'row_field' => 'type',
                    'sorting_field' => 'banner.type',
                    'sortable' => false,
                ],
                'a.pic' => [
                    'title' => 'Pic',
                    'row_field' => 'pic',
                    'sorting_field' => 'banner.pic',
                    'sortable' => false,
                ],
                'a.isActive' => [
                    'title' => 'Is active?',
                    'row_field' => 'isActive',
                    'sorting_field' => 'banner.isActive',
                    'sortable' => false,
                ],
            ]
        ));
    }


    /**
     * Creates a new banner entity.
     *
     * @Route("backend/banner/new", name="backend_banner_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, ValidatorInterface $validator)
    {
        $banner = new Banner();
        $form = $this->createForm('App\Form\BannerType', $banner);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($banner);
            $em->flush($banner);
            $this->addFlash('success', 'New record was created!');

            return $this->redirectToRoute('backend_banner_edit', array('id' => $banner->getId()));
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'Errors due creating object!');
        }

        return $this->render('admin/common/new.html.twig', array(
            'row' => $banner,
            'form' => $form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,

        ));
    }

    /**
     * Finds and displays a banner entity.
     *
     * @Route("backend/banner/{id}", name="backend_banner_show")
     * @Method("GET")
     */
    public function showAction(Banner $banner)
    {
        $deleteForm = $this->createDeleteForm($banner);

        return $this->render('admin/banner/show.html.twig', array(
            'banner' => $banner,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing banner entity.
     *
     * @Route("backend/banner/{id}/edit", name="backend_banner_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Banner $banner, FileUploader $fileUploader)
    {
        $deleteForm = $this->createDeleteForm($banner);
        $editForm = $this->createForm('App\Form\BannerType', $banner);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            /*
             * 1. Если в форме оставить только pic, то при отправке формы без фото, поле pic стирается
             * 2. Если добавить picFile, которые не хранится в базе, то получается что при изменении только
             * фото, модель не считается изменённой, и вообще не сохраняется, не срабатывают Listeners
             * как вариант, в таком случае менять UpdatedAt, и дальше пусть работает Listener
             */
            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $banner->getPicFile();

            if (null !== $file) {
                $fileName = $fileUploader->upload($file);
                $banner->setPic($fileName);
            }

            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Your changes were saved!');

            return $this->redirectToRoute('backend_banner_edit', array('id' => $banner->getId()));
        }
        if ($editForm->isSubmitted() && !$editForm->isValid()) {
            $this->addFlash('danger', 'Errors due saving object!');
        }

        return $this->render('admin/banner/edit.html.twig', array(
            'row' => $banner,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
        ));
    }

    /**
     * Deletes a banner entity.
     *
     * @Route("backend/banner/{id}", name="backend_banner_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Banner $banner)
    {
        $filter_form = $this->createDeleteForm($banner);
        $filter_form->handleRequest($request);

        if ($filter_form->isSubmitted() && $filter_form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($banner);
            $em->flush($banner);

            $this->addFlash('success', 'Record was successfully deleted!');
        }

        if (!$filter_form->isValid()) {
            /** @var FormErrorIterator $errors */
            $errors = $filter_form->getErrors()->__toString();
            $this->addFlash('danger', 'Error due deletion! ' . $errors);
        }

        return $this->redirectToRoute('backend_banner_index');
    }

    /**
     * Creates a form to delete a banner entity.
     *
     * @param Banner $banner The banner entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Banner $banner)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_banner_delete', array('id' => $banner->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }


}
