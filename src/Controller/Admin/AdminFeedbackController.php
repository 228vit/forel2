<?php

namespace App\Controller\Admin;

use App\Entity\Feedback;
use App\Filter\FeedbackFilter;
use App\Repository\FeedbackRepository;
use Doctrine\ORM\Query;
use Knp\Component\Pager\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class AdminFeedbackController extends AbstractController
{
    use AdminTraitController;

    CONST ROWS_PER_PAGE = 10;
    CONST MODEL = 'feedback';
    CONST ENTITY_NAME = 'Feedback';
    CONST NS_ENTITY_NAME = 'App:Feedback';

    /**
     * Lists all feedback entities.
     *
     * @Route("backend/feedback/index", name="backend_feedback_index")
     * @Method("GET")
     */
    public function indexAction(Request $request, SessionInterface $session)
    {
        $pagination = $this->getPagination($request, $session, FeedbackFilter::class);

        return $this->render('admin/common/index.html.twig', array(
            'pagination' => $pagination,
            'current_filters' => $this->current_filters,
            'filter_form' => $this->filter_form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
            'list_fields' => [
                'a.id' => [
                    'title' => 'ID',
                    'row_field' => 'id',
                    'sorting_field' => 'feedback.id',
                    'sortable' => true,
                ],
                'a.name' => [
                    'title' => 'Name',
                    'row_field' => 'name',
                    'sorting_field' => 'feedback.name',
                    'sortable' => true,
                ],
                'a.email' => [
                    'title' => 'Email',
                    'row_field' => 'email',
                    'sorting_field' => 'feedback.email',
                    'sortable' => false,
                ],
                'a.phone' => [
                    'title' => 'Phone',
                    'row_field' => 'phone',
                    'sorting_field' => 'feedback.phone',
                    'sortable' => false,
                ],
                'a.message' => [
                    'title' => 'Message',
                    'row_field' => 'message',
                    'sorting_field' => 'feedback.id',
                    'sortable' => false,
                ],
            ]
        ));
    }


    /**
     * Creates a new feedback entity.
     *
     * @Route("backend/feedback/new", name="backend_feedback_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, ValidatorInterface $validator)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPERADMIN', null, 'Unable to access this page!');

        $feedback = new Feedback();
        $form = $this->createForm('App\Form\FeedbackType', $feedback);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($feedback);
            $em->flush($feedback);
            $this->addFlash('success', 'New record was created!');

            return $this->redirectToRoute('backend_feedback_edit', array('id' => $feedback->getId()));
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'Errors due creating object!');
        }

        return $this->render('admin/common/new.html.twig', array(
            'feedback' => $feedback,
            'form' => $form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,

        ));
    }

    /**
     * Finds and displays a feedback entity.
     *
     * @Route("backend/feedback/{id}", name="backend_feedback_show")
     * @Method("GET")
     */
    public function showAction(Feedback $feedback)
    {
        $deleteForm = $this->createDeleteForm($feedback);

        return $this->render('admin/feedback/show.html.twig', array(
            'feedback' => $feedback,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing feedback entity.
     *
     * @Route("backend/feedback/{id}/edit", name="backend_feedback_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Feedback $feedback)
    {
        $deleteForm = $this->createDeleteForm($feedback);
        $editForm = $this->createForm('App\Form\FeedbackType', $feedback);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Your changes were saved!');

            return $this->redirectToRoute('backend_feedback_edit', array('id' => $feedback->getId()));
        }
        if ($editForm->isSubmitted() && !$editForm->isValid()) {
            $this->addFlash('danger', 'Errors due saving object!');
        }

        return $this->render('admin/common/edit.html.twig', array(
            'row' => $feedback,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
        ));
    }

    /**
     * Deletes a feedback entity.
     *
     * @Route("backend/feedback/{id}", name="backend_feedback_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Feedback $feedback)
    {
        $filter_form = $this->createDeleteForm($feedback);
        $filter_form->handleRequest($request);

        if ($filter_form->isSubmitted() && $filter_form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($feedback);
            $em->flush($feedback);

            $this->addFlash('success', 'Record was successfully deleted!');
        }

        if (!$filter_form->isValid()) {
            /** @var FormErrorIterator $errors */
            $errors = $filter_form->getErrors()->__toString();
            $this->addFlash('danger', 'Error due deletion! ' . $errors);
        }

        return $this->redirectToRoute('backend_feedback_index');
    }

    /**
     * Creates a form to delete a feedback entity.
     *
     * @param Feedback $feedback The feedback entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Feedback $feedback)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_feedback_delete', array('id' => $feedback->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }


}
