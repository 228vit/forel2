<?php

namespace App\Controller\Admin;

use App\Entity\CategoryFilter;
use App\Entity\CategoryFilterValue;
use App\Filter\CategoryFilterFilter;
use App\Form\CategoryFilterType;
use App\Repository\CategoryFilterRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Form\FormView;

/**
 * @Route("/backend/category_filter")
 */
class AdminCategoryFilterController extends AbstractController
{
    use AdminTraitController;

    CONST ROWS_PER_PAGE = 10;
    CONST MODEL = 'category_filter';
    CONST ENTITY_NAME = 'CategoryFilter';
    CONST NS_ENTITY_NAME = 'App:CategoryFilter';

    /**
     * @Route("/", name="backend_category_filter_index", methods={"GET"})
     */
    public function index(Request $request, SessionInterface $session): Response
    {
        // todo: проверить не перепутаются ли фильтры
        $pagination = $this->getPagination($request, $session, CategoryFilterFilter::class);

        return $this->render('admin/common/index.html.twig', array(
            'pagination' => $pagination,
            'current_filters' => $this->current_filters,
            'filter_form' => $this->filter_form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
            'list_fields' => [
                'a.id' => [
                    'title' => 'ID',
                    'row_field' => 'id',
                    'sorting_field' => 'faq.id',
                    'sortable' => true,
                ],
                'a.category' => [
                    'title' => 'Category',
                    'row_field' => 'category',
                    'sorting_field' => 'category_filter.category',
                    'sortable' => false,
                ],
                'a.name' => [
                    'title' => 'Name',
                    'row_field' => 'name',
                    'sorting_field' => 'category_filter.name',
                    'sortable' => true,
                ],
                'a.position' => [
                    'title' => 'Position',
                    'row_field' => 'position',
                    'sorting_field' => 'category_filter.position',
                    'sortable' => true,
                ],
            ]
        ));
    }

    /**
     * @Route("/new", name="backend_category_filter_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $row = new CategoryFilter();
        $form = $this->createForm(CategoryFilterType::class, $row);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($row);
            $entityManager->flush();

            return $this->redirectToRoute('backend_category_filter_index');
        }

        return $this->render('admin/common/new.html.twig', array(
            'form' => $form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,

        ));
    }


    /**
     * @Route("/{id}/edit", name="backend_category_filter_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, CategoryFilter $row): Response
    {
        $originalFilterValues = new ArrayCollection();

        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($row->getFilterValues() as $filterValue) {
            $originalFilterValues->add($filterValue);
        }

        $form = $this->createForm(CategoryFilterType::class, $row);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $filterValues = $form['filterValues']->getData();
            /** @var CategoryFilterValue $filterValue */
            foreach ($filterValues as $i => $filterValue) {
                $filterValue->setCategoryFilter($row);
                if (false === $originalFilterValues->contains($filterValue)) {
                    $originalFilterValues->add($filterValue);
                }
            }

            // kill removed
            foreach ($originalFilterValues as $removedItem) {
                if (false === $row->getFilterValues()->contains($removedItem)) {
                    $row->getFilterValues()->removeElement($removedItem);
                    $this->getDoctrine()->getManager()->remove($removedItem);
                }
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('backend_category_filter_edit', array('id' => $row->getId()));
        }

        $deleteForm = $this->createDeleteForm($row);

        return $this->render('admin/category_filter/edit.html.twig', array(
            'row' => $row,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
        ));
    }

    /**
     * @Route("/{id}", name="backend_category_filter_delete", methods={"DELETE"})
     */
    public function delete(Request $request, CategoryFilter $categoryFilter): Response
    {
        if ($this->isCsrfTokenValid('delete'.$categoryFilter->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($categoryFilter);
            $entityManager->flush();
        }

        return $this->redirectToRoute('category_filter_index');
    }

    /**
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CategoryFilter $row)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_category_filter_delete', array('id' => $row->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }

}
