<?php

namespace App\Controller\Admin;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminController extends AbstractController
{
    /**
     * @Route("/backend", name="backend_dashboard")
     */
    public function index()
    {
        return $this->render('admin/dashboard.html.twig', [
        ]);
    }

    /**
     * @Route("/backend/data", name="backend_data_example")
     */
    public function dataExample()
    {
        return $this->render('admin/table_example.html.twig', [
        ]);
    }

}
